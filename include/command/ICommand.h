#ifndef ICOMMAND_H_
#define ICOMMAND_H_

namespace command
{
    class ICommand
    {
    public:
        virtual ~ICommand() = default;

        virtual void execute() = 0;
    };
} // namespace command
#endif ICOMMAND_H_