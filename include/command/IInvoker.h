#ifndef IINVOKER_H_
#define IINVOKER_H_

namespace command
{
    class IInvoker
    {
        virtual ~IInvoker() {}
    };

} // namespace command

#endif /* IINVOKER_H_ */