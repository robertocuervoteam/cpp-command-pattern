#ifndef IRECEIVER_H_
#define IRECEIVER_H_
namespace command
{
    class IReceiver
    {
        ~IReceiver() {}
        virtual void action() = 0;
    };
} //namespace command
#endif /* IRECEIVER_H_ */